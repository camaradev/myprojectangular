export interface MenuModel {
  url:string;
  name:string;
  minimumAge:number;
}
