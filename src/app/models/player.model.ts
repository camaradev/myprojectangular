export interface PlayerModel {
    id: number;
    firstName: string;
    lastName: string;
    age: number;
    teamNumber: number;
}
