import {PlayerModel} from './player.model';
export interface TeamModel {
    id?: number | string;
    city: string;
    playersNumbers: number;
    stadium: boolean;
    players?: number[];
    playersData?: PlayerModel[];
    subscribers?: number;
}
