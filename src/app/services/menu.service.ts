import { Injectable } from '@angular/core';
import {MenuModel} from "../models/menu.model";

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  allRoutes: MenuModel[]=[
    {url:'home', name:'accueil', minimumAge:0},
    {url:'à-propos', name:'à propos', minimumAge:32},
    {url:'form', name:'formulaire', minimumAge:0}
  ];

  constructor() { }
}
