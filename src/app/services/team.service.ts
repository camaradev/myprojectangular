import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {TeamModel} from "../models/team.model";
import {tap} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class TeamService {

  root:string=environment.api;

  constructor(
    private httpClient:HttpClient,
  ) {

  }

  getTeamById$(teamId): Observable<TeamModel>{
    return this.httpClient.get(this.root+teamId) as
      Observable<TeamModel>;
  }

  getAllTeams$():Observable<TeamModel[]>{
    return this.httpClient.get(this.root+'teams') as
      Observable<TeamModel[]>;
  }

  createNewTeam$(team:TeamModel):Observable<TeamModel>{
    return (this.httpClient
      .post(this.root+'teams', team) as
      Observable<TeamModel>).pipe(
        tap(x=>console.log(x)),
    );
  }

  deleteTeam$(teamId: string): Observable<string>{
    return (this.httpClient
      .delete(this.root+'teams/'+teamId) as
      Observable<string>).pipe(
      tap(x=>console.log(x)),
    );
  }
}
