import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuModel} from "./models/menu.model";
import {MenuService} from "./services/menu.service";
import {MoodService} from "./services/mood.service";
import {takeUntil, tap} from "rxjs/operators";
import {combineLatest, Subject} from "rxjs";
import {TeamService} from "./services/team.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'promo3Angular';
  message:string;

  destroy$:Subject<boolean>=new Subject();

  allRoutes:MenuModel[]=this.menuService.allRoutes;
  age=34;

  constructor(
    private menuService: MenuService,
    private moodService: MoodService,
    private teamService: TeamService,
  ) {
  }

  ngOnInit(): void {

    combineLatest([
      this.teamService.getAllTeams$()
        .pipe(
          tap(x=>console.log(x)),
        ),
      this.moodService.handleMood$
        .pipe(
          tap((message:string)=>this.message=message),
        )
    ])
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  createTeam(): void{
    this.teamService.createNewTeam$({
      city: 'psg b',
      playersNumbers: 23,
      stadium: false,
    }).subscribe();
  }

  deleteTeam(): void{
    this.teamService.deleteTeam$('skUTgZ2UlLVlXlRIZGgr').subscribe();
  }

}
