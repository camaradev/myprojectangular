import {Component,  OnInit} from '@angular/core';
import {MoodService} from "../../services/mood.service";

@Component({
  selector: 'app-mood',
  templateUrl: './mood.component.html',
  styleUrls: ['./mood.component.scss']
})
export class MoodComponent implements OnInit {

  constructor(private moodService:MoodService) {

  }

  ngOnInit(): void {
  }

  iAmOk(){
    this.moodService.setMood('je suis ok');
  }
  iAmNotOk(){
    this.moodService.setMood('je suis pas ok');
  }
  iAmNeutral(){
    this.moodService.setMood('je suis neutre');
  }
}
