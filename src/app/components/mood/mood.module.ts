import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoodComponent } from './mood.component';



@NgModule({
  declarations: [MoodComponent],
  exports: [MoodComponent],
  imports: [
    CommonModule
  ]
})
export class MoodModule { }
